from BCS import solver, Chebyshev_vector
from matplotlib import pyplot as plt
import numpy as np
import scipy.linalg

#These are the constants defined after the differential equation.
Gamma = 5/3
v_plus = 3/5
v_star = Gamma / (Gamma +2)
e_neg = ((Gamma +2)*(v_plus - v_star))/(2*Gamma*(Gamma +1))


def RHS(x):
    """
    The right-hand side for the differential equation given in the midterm.
    It's labeled as equation (1) on the handout.
    """
    #Have v and e be the first and second entries in the array.
    v = x[0]
    e = x[1]
    
    #This is equation (1)
    dvdt = v*(v-1) + Gamma*(e - v*e_neg)
    dedt = v*((-(v-1)**2)/2 + e - e_neg + Gamma*e_neg*(v-1))
    
    return np.array((dvdt, dedt))
    
def J_RHS(x):
    """
    Jacobian of the right hand side function for our differential equation.
    """
    v = x[0]
    e = x[1]
    return np.array(
        [[2*v-1-Gamma*e_neg,                                            Gamma],
        [-3/2*(v-1)**2 + (2*Gamma*e_neg - 1)*(v-1) + (Gamma -1)*e_neg + e, v]])

def bc(ya, yb):
    va = x[0]
    ea = x
