import numpy as np
from BCS import solver, Chebyshev_vector
from matplotlib import pyplot as plt
"""
Test on the differential equation
y'' + y = 0
y(0) = 0
y(5pi/2) = 1

The solution to this is sin(x).  We should expect that as the result of this process.

The differential equation can be changed to 
[y1]' = [0,  1]
[y2]  = [-1, 0]
"""
def RHS_0(x):
    return np.array([[0, 1], [-1, 0]]).dot(x)

def Jac_RHS_0(x):
    return np.array([[0, 1], [-1, 0]])
    
def bc_0(ya, yb):
    error_1 = np.array([ya[0], 0])
    error_2 = np.array([yb[0]-1, 0])
    return np.hstack((error_1, error_2))

def Jac_bc_0(ya, yb):
    return np.array(
        [[1, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 0]])

interval_0 = np.array([0, 5*np.pi/2])

n_0 = 2

test_solver0 = solver(RHS_0, bc_0, interval_0, n_0, Jac_RHS=Jac_RHS_0,
                        Jac_bc=Jac_bc_0, initialK = 16)

soln = test_solver0.solve()

t = np.linspace(0, 3*np.pi, 1000)

plt.plot(t, soln[0](t), label="Estimated value for $y$")
plt.plot(t, np.sin(t), 'k:',  label="True value of $y$")
plt.legend()
plt.show()
