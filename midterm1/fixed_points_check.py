print()
#Try to find the values of v numerically
from math import sqrt

#Define the constants
Gamma = 5/3
v_star = (Gamma)/(Gamma +2)
v_plus = 3/5
e_neg = ((Gamma +2)*(v_plus - v_star))/(2*Gamma*(Gamma+1))

#Prepare to use the quadratic formula.  Use the coefficients I found in my work.
a = (1+Gamma/2)
b = -(1 + Gamma * e_neg + Gamma + (Gamma**2)*e_neg)
c = Gamma*e_neg + Gamma/2 +(Gamma**2) * e_neg

#Time to quadratic formula it up.
solutions = [(-b + pm*sqrt(b**2 - 4*a*c))/(2*a) for pm in [1, -1]]

print("Using floating point arithmetic, these are the equilibrium values of v:")
for soln in solutions:
    print(soln)

print()
#Also try to find the values of v symbolically.
import sympy as sy

#Do the exact same stuff, just make sympy do everything.
Gamma = sy.Rational(5,3)
v_star = Gamma/(Gamma+2)
v_plus = sy.Rational(3, 5)
e_neg = ((Gamma+2)*(v_plus-v_star))/(2*Gamma*(Gamma+1))

a = (1+Gamma/2)
b = -(1 + Gamma * e_neg + Gamma + (Gamma**2) * e_neg)
c = Gamma*e_neg + Gamma/2 + (Gamma**2) * e_neg

solutions = [((-b + pm*sy.sqrt(b**2 - 4*a*c))/(2*a)).simplify() for pm in [1, -1]]

print("Using sympy these are the equilibrium values of v:")
for soln in solutions:
    print(soln, ", which is approximately", float(soln))
    
print()
