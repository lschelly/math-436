from BCS import solver, Chebyshev_vector
from matplotlib import pyplot as plt
import numpy as np
import scipy.linalg

#These are the constants defined after the differential equation.
Gamma = 5/3
v_plus = 3/5
v_star = Gamma / (Gamma +2)
e_neg = ((Gamma +2)*(v_plus - v_star))/(2*Gamma*(Gamma +1))


def RHS_org(x):
    """
    The right-hand side for the differential equation given in the midterm.
    It's labeled as equation (1) on the handout.
    """
    #Have v and e be the first and second entries in the array.
    v = x[0]
    e = x[1]
    
    #This is equation (1)
    dvdt = v*(v-1) + Gamma*(e - v*e_neg)
    dedt = v*((-(v-1)**2)/2 + e - e_neg + Gamma*e_neg*(v-1))
    
    return np.array((dvdt, dedt))
    
def J_RHS_org(x):
    """
    Jacobian of the right hand side function for our differential equation.
    """
    v = x[0]
    e = x[1]
    return np.array(
        [[2*v-1-Gamma*e_neg,                                            Gamma],
        [-3/2*(v-1)**2 + (2*Gamma*e_neg - 1)*(v-1) + (Gamma -1)*e_neg + e, v]])

#To account for doing a linear shift, we need to define a new right hand
#side that operates on R^4
def RHS(x):
    """
    Breaks the length 4 array x into two different arrays of size 2.
    It then applies RHS_for_project to both.
    """
    x1 = x[:2]
    x2 = x[2:]
    return np.hstack([RHS_org(u) for u in (x1, x2)])
    
def J_RHS(x):
    """
    Jacobian of RHS
    """
    x1 = x[:2]
    x2 = x[2:]
    return scipy.linalg.block_diag(J_RHS_org(x1), J_RHS_org(x2))
    
    
#Now move on to making the boundary conditions.
#First, make the boundary condition where we approach along an eigenvector.

saddle_point = np.array(
[v_plus, (v_plus - 1)**2 / 2 - Gamma*e_neg*(v_plus -1) + e_neg])

#Find the eigenvalues and eigenvectors at the saddle point.
evals, evecs = np.linalg.eig(J_RHS_org(saddle_point))

#Find the eigenvector for the negative eigenvalue.
index = evals.argsort()[0] #Find index of smallest eigenvalue.
assert evals[index] < 0 #Check to make sure the smallest eigenvalue is negative.
neg_evec = evecs[:,index] #Then grab it's eigenvector

#Find a vector perpendicular to that negative eigenvector.
perp_vec = np.array([-1, 1])*neg_evec[::-1]

def boundary_conditions(x0, xL):
    #Grab the first two entries of xL.
    #They should be the position of our particle at time L.
    x10 = x0[:2]
    x20 = x0[2:]
    x1L = xL[:2]
    x2L = xL[2:]

    #Use a dot product with the vector perpendicular to the eigenvector
    #We need that to be zero.
    dot_result = (saddle_point - x1L).dot(perp_vec)

    #For our phase condition, have v1 = x10[0] be at the midpoint of 1 and v_plus
    phase_condition = x10[0] - (1+v_plus)/2

    #We want x1(t) = x2(t+L), so we need x1(0) = x2(L).
    #Dr. Barker called this a linear shift.
    linear_shift = x10 - x2L

    #Append 4 zeros, because the solver expects an array of size 8.
    return np.hstack((
        np.array([dot_result, phase_condition]),
        linear_shift, 
        np.zeros(4)))

def J_bc(x0, xL):
    """
    Jacobian of our boundary conditions.
    """
    #For keeping track of rows/columns
    #x[0] = x10[0]
    #x[1] = x10[1]
    #x[2] = x20[0]
    #x[3] = x20[1]
    #x[4] = x1L[0]
    #x[5] = x1L[1]
    #x[6] = x2L[0]
    #x[7] = x2L[1]
    J = np.vstack((
        #Dot product is only first entry, and affects only x1L
        np.hstack((np.zeros(4), -perp_vec, np.zeros(2))),
        #Phase condition only affects x10[0]
        [1, 0, 0, 0, 0, 0, 0, 0],
        #Linear shift affects x10 and x2L, and takes up two entries
        [1, 0, 0, 0, 0, 0, -1, 0],
        [0, 1, 0, 0, 0, 0, 0, -1],
        *([np.zeros(8)]*4)))#4 remaining rows are zero.
    return J

#Define the interval we will solve over.
L = 100
interval = np.array([0, L])
#Dimension should be 4
dimension = 4

print("\nInstantiating a solver object")
solver2 = solver(RHS, boundary_conditions, interval, dimension, 
                    Jac_RHS=J_RHS, Jac_bc=J_bc, initialK = 128)
print("Having the solver object try to solve the system")
soln = solver2.solve(maxiters=1)
print("This is the coefficient array for the solution:")
print(soln.getA())
