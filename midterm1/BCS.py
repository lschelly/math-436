import numpy as np
import numpy.polynomial.chebyshev as cheb
from scipy.optimize import fsolve
import scipy.linalg as la
from matplotlib import pyplot as plt

class Chebyshev_vector:
    """
    Convenience class for dealing with functions from R to R^n
    where each entry is a finite linear combination of chebyshev polynomialss.
    """
    
    def __init__(self, A, interval):
        #Just make a list with all the chebyshev polynomials in it.
        #Each row of A should have the correct coefficients.
        self.entries = [cheb.Chebyshev(row, interval) for row in A]
        
        
    def __call__(self, t):
        #Evaluate each entry at t, and then return the vector of evaluations.
        return np.array([f(t) for f in self.entries])
    
    def __getitem__(self, i):
        return self.entries[i]
    
    def setA(self, Ain):
        """
        Accomodates any value of Ain where Ain can be reshaped to the right size.
        """
        
        #Figure out the number of rows and columns we should expect
        n = len(self.entries) #number of rows
        K = len(self.entries[0].coef) #number of columns
        
        #Reshape the input to have that number of rows and columns.
        A = Ain.reshape((n, K))
        
        #Set the coefficients of each entry in our list of Chebyshev polynomials.
        for entry, row in zip(self.entries, A):
            entry.coef = row
        return
    
    def getA(self):
        #Use the other method we wrote.
        return self._getAFromEntries(self.entries)
    
    def _getAFromEntries(self, entriesParam):
        #Go through each Chebyshev polynomial and put its coefficients in a row.
        return np.array([entry.coef for entry in entriesParam])
        
    def getInterval(self):
        #Just get the domain of the first entry.  They should all be the same.
        return self.entries[0].domain
    
    def deriv(self):
        #Make a list of the derivatives of all the entries.
        derivList = [entry.deriv() for entry in self.entries]
        #Throw that list of entries into the method for getting A from the entries.
        derivA = self._getAFromEntries(derivList)
        #Create the derivative from that matrix and the interval of this thing.
        return Chebyshev_vector(derivA, self.getInterval())
    

class solver:
    
    def __init__(self, RHS, bc, interval, n, Jac_RHS=None, 
                    Jac_bc=None,  etol=2**-5, initialK = 4):
        """
        RHS -- Right hand side of the differential equaion.
        bc -- function handle for the boundary conditions.  Works like in bvp_solve
        interval -- np.array of size 2.
        n -- dimension of the system.
        etol -- error tolerance
        initialK -- initial number of Chebyshev polynomials to use.
        """
    
        self.RHS = RHS
        self.bc = bc
        
        #Make sure they didn't pass in a bogus interval.
        assert interval.size == 2 and interval[0] < interval[1]
      
        self.interval = interval
        self.n = n
        self.Jac_RHS = Jac_RHS
        self.Jac_bc = Jac_bc
        self.etol = etol
        self.K = np.max((initialK, int(2**np.ceil(np.log2(n)))))
        
        #Make the collocation points.
        self.collocation_points = self.getChebyshevPoints()
        
    
    def solve(self, maxiters=3):
        """
        Return a chebyshev vector that is close enough to collocating and 
        close enough to solving the boudnary conditions.
        """
        #Each iteration is another guess at the number of Chebyshev polynomials.
        for iter in range(1, maxiters+1):
            
            #Start out with an initial guess for what the matrix of coefficients will be
            A = np.zeros((self.n, self.K))
            A[:,0] = 1 #Assume they're all constant.
            
            #Calculate S[A]
            self.S = Chebyshev_vector(A, self.interval)
            
            #Determine what we should use as the jacobian of H.
            jacobian = None
            if(self.Jac_RHS != None and self.Jac_bc != None):
                jacobian = self.Jac_H
            
            #Apply fsolve to the raveled version of A.
            raveled = fsolve(self.H, A.ravel(), fprime = jacobian)
            
            #Undo the raveling, and store the coefficients.
            A = raveled.reshape(self.n, self.K)
            self.S.setA(A)
            
            #Calculate the max error
            P = np.linspace(self.interval[0], self.interval[1], 1000)
            Sprime = self.S.deriv()
            error = np.max([np.linalg.norm(Sprime(p)-self.RHS(self.S(p))) for p in P])
            
            if(error < self.etol):
                return self.S
            else:
                print("solver: Failed with %d polynomials"%self.K)
                print("solver: This is the coefficient matrix:")
                print("%r"%self.S.getA())
                #Double the number of collocation points
                self.K*=2
                self.collocation_points = self.getChebyshevPoints()
            
        print("Maximum number of iterations exceeded.")
        return self.S
    
    def H(self, Ain):
        """
        For telling fsolve how far we are off at each collocation point,
        and how far we are off at each boundary condition.
        """
        #print("solver.H: Called with this value of A:", Ain)
        #Readjust the matrix we have plugged into S[A].
        self.S.setA(Ain)
        #Find its derivative.
        Sprime = self.S.deriv()
        
        #Get the collocation error at the Chebyshev points.
        roots = self.collocation_points
        root_errors = np.hstack([Sprime(r)- self.RHS(self.S(r)) for r in roots])
        
        #Get the error at the boundary points.
        t_1, t_2 = self.interval
        bc_error = self.bc(self.S(t_1), self.S(t_2))
        
        return np.hstack((root_errors, bc_error))
        
    def Jac_H(self, Ain):
        """
        The Jacobian matrix of the frechet derivative of H.
        """
        #Make sure no goofball is calling this without supplying the stuff we need.
        assert self.Jac_RHS != None and self.Jac_bc != None
        
        n = self.n
        K = self.K
        
        #Adjust S.
        A = Ain.reshape(n, K)
        self.S.setA(A)
        
        #Initialize an empty matrix of size n*K by n*K to hold the jacobian.
        JH = np.empty((n*K, n*K))
        
        #Make a vector of size K where the ith entry is the ith Chebyshev polynomial.
        T = Chebyshev_vector(np.eye(K), self.interval)
        Tp = T.deriv()
        
        #The first K-2 blocks of rows of size n will be determined as follows:
        #For each collocation point ...
        for i, collocation_point in enumerate(self.collocation_points):
            #Let i be the index of the collocation point
            #Label the collocation point as r
            r = collocation_point
            
            #Find the Jacobian for (S[A])'(r)
            derivative_part = la.block_diag(*([Tp(r)]*n))
            
            #Find the Jacobian for RHS(S[A](r))
            RHS_part = self.Jac_RHS(self.S(r)).dot(la.block_diag(*([T(r)]*n)))
            
            #The ith group of n rows of JH should be the difference of
            #the derivative part with the RHS part.
            JH[i*n :(i+1)*n] = derivative_part - RHS_part
        
        #The last two blocks of n rows will be determined from the boundary conditions.
        t0, t1 = self.interval
        Jac_bc = self.Jac_bc(self.S(t0), self.S(t1))
        double_stacked = np.vstack((la.block_diag(*([T(t)]*n)) for t in [t0, t1]))
        JH[(K-2)*n:K*n] = Jac_bc.dot(double_stacked)
        print("Solver: Jacobian called")
        return JH
          
    
    def getChebyshevPoints(self):
        #See the Wikipedia article on Chebyshev nodes for this formula
        normal_roots = np.cos((2*np.arange(1, self.K-1) -1)/(2*self.K) * np.pi)
        #There are 2 less roots than K, so we have room for 2 boundary conditions.
        a = self.interval[0]
        b = self.interval[1]
        shifted_roots = (a+b)/2 + (b-a)*normal_roots/2 #also from Wikipedia
        
        return shifted_roots
        

