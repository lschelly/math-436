import numpy as np
from scipy.optimize import fsolve

def tf(f,x):
    if np.isscalar(f(x)):
        return np.array(f(x))
    return f(x)

def f(TS1,TS0,K1,K2,K3,ct1,ct2,ct3,ct4,cs1,cs2,cs3,cs4,A,B,C1,C2,N_x, vals):
    """The nonlinear implicit Crank-Nicholson equations for 
    wildfire model.
    
    Parameters
    ----------
        TS1 (ndarray): The values of U^(n+1)
        TS0 (ndarray): The values of U^n
        K1 (float): first constant in the equations
        K2 (float): second constant in the equations
        K3 (float): thrid constant in the equations
    
    Returns
    ----------
        out (ndarray): The residuals (differences between right- and 
                    left-hand sides) of the equation, accounting 
                    for boundary conditions
    """
    # initialize array of residuals
    Terr = np.zeros(N_x)
    Serr = np.zeros(N_x)
    
    # get T1, T0, S1, S0
    T1 = TS1[::2]
    T0 = TS0[::2]
    S1 = TS1[1::2]
    S0 = TS0[1::2]

    hTa, cTa, dTa, hSa, cSa, dSa, hTb, cTb, dTb, hSb, cSb, dSb = vals
    
    # define terms used to fill in interior of T
    term1 = T0[2:]-2*T0[1:-1]+T0[:-2] + T1[2:] - 2*T1[1:-1]+T1[:-2]
    term2 = T0[2:]-T0[:-2]+T1[2:]-T1[:-2]
    term3 = A*.5*((S1[1:-1]+S0[1:-1])*np.exp(-2*B/(T1[1:-1]+T0[1:-1])) - C1*(T1[1:-1]+T0[1:-1]))
    
    # fill in interior of T
    Terr[1:-1] = T1[1:-1] - T0[1:-1] - K1*term1 - K2*term2 - K3*term3
    
    # fill in boundaries of T
    #Terr[0] = T1[0] - (ct1 + ct2*T1[1])
    #Terr[-1] = T1[-1] - (ct3 + ct4*T1[-2])
    dt = K3
    Terr[0] = -hTa + cTa*(T1[0]) + dTa*(T1[1]-T1[0])/dt
    Terr[-1] = -hTb + cTb*(T1[-1]) + dTb*(T1[-2]-T1[-1])/dt
    
    # fill in interior of S
    Serr[1:-1] = S1[1:-1] - S0[1:-1] + .5*K3*C2*(S1[1:-1]+S0[1:-1])*np.exp(-2*B/(T1[1:-1]+T0[1:-1]))
    
    Serr[0] = -hSa + cSa*(S1[0]) + dSa*(S1[1]-S1[0])/dt
    Serr[-1] = -hSb + cSb*(S1[-1]) + dSb*(S1[-2]-S1[-1])/dt
    # fill in boundaries of S
    #Serr[0] = S1[0] - (cs1 + cs2*S1[1])
    #Serr[-1] = S1[-1] - (cs3 + cs4*S1[-2])
    
    # interweave T and S
    TSerr = np.zeros(2*N_x)
    TSerr[::2] = Terr
    TSerr[1::2] = Serr
    
    return TSerr
    
def wildfire_model(a,b,Time,N_x,N_t,T_0,S_0,cT_a,dT_a,hT_a,cT_b,
                    dT_b,hT_b,cS_a,dS_a,hS_a,cS_b,dS_b,hS_b,A,B,C1,C2,nu):
    """
    Solver for simple wildfire model. Uses Crank Nicholson Scheme to solve for 
    temperature and fuel.
    Parameters:
        a (float) - right spatial boundary
        b (float) - left spatial boundary
        Time (float) - end time value; must be strictly positive
        N_x (int) - number of mesh nodes in x; must be greater than 2
        N_t (int) - number of mesh nodes in t; must be greater than 1
        T_0 (function handle) - initial function auxiliary condition for temp
        S_0 (function handle) - initial funtion auxiliary condition for fuel
        cT_a (function handle) - function of t for temp
        dT_a (function handle) - functino of t for temp
        hT_a (function handle) - right boundary condition function for temp
        cT_b (function handle) - function of t for temp
        dT_b (function handle) - function of t for temp
        hT_b (function handle) - left boundary condition function for temp
        cS_a (function handle) - function of t for fuel
        dS_a (function handle) - function of t for fuel
        hS_a (function handle) - right boundary condition function for fuel
        cS_b (function handle) - function of t for fuel
        dS_b (function handle) - function of t for fuel
        hS_b (function handle) - left boundary condition function for fuel
        A (float) - constant
        B (float) - constant
        C1 (float) - constant
        C2 (flost) - constant
        nu (float) - constant of wind conditions
    Returns: 
        T (2d numpy array) - solution for tempurature
        S (2d numpy array) - solution for fuel
    """
    
    # set up linspace for spatial dimension
    x,h = np.linspace(a,b,N_x,retstep=True)
    
    # set up linspace for temporal dimension
    t,dt = np.linspace(0, Time, N_t, retstep=True)
    
    # initialize tensor to store solution
    TnS = np.zeros((2, N_t, N_x))
    
    # get first time step for temperature and fuel
    TnS[0,0,:] = tf(T_0, x)
    TnS[1,0,:] = tf(S_0, x)
    
    # define constants
    K1 = dt/(2*h**2)
    K2 = -(nu*dt)/(4*h)
    K3 = dt
    
    # iterate through remaining time steps, using fsolve to solve for each successive step
    for j in range(1, N_t):
        # get specific t
        tn = t[j]
        
        hTa, cTa, dTa = hT_a(tn), cT_a(tn), dT_a(tn)
        hSa, cSa, dSa = hS_a(tn), cS_a(tn), dS_a(tn)
        hTb, cTb, dTb = hT_b(tn), cT_b(tn), dT_b(tn)
        hSb, cSb, dSb = hS_b(tn), cS_b(tn), dS_b(tn)
        vals = [hTa, cTa, dTa, hSa, cSa, dSa, hTb, cTb, dTb, hSb, cSb, dSb] 
        
        
        # calculate specific constants for T
        ct1 = hT_a(tn) / (cT_a(tn) - (dT_a(tn)/h))
        ct2 = dT_a(tn) / (h*cT_a(tn) - dT_a(tn))
        ct3 = hT_b(tn) / (h*cT_b(tn) + (dT_b(tn)/h))
        ct4 = dT_b(tn) / (h*cT_b(tn) + dT_b(tn))
        
        # calculate specific constants for S
        cs1 = hS_a(tn) / (cS_a(tn) - (dS_a(tn)/h))
        cs2 = dS_a(tn) / (h*cS_a(tn) - dS_a(tn))
        cs3 = hS_b(tn) / (h*cS_b(tn) + (dS_b(tn)/h))
        cs4 = dS_b(tn) / (h*cS_b(tn) + dS_b(tn))

        # solve the nonlinear system for T and S at each time step, storing the solution
        ts = TnS[:,j-1,:]
        guess = np.zeros(2*N_x)
        guess[::2] = ts[0]
        guess[1::2] = ts[1]
        #print(f"this is the guess for time step: {j}: {guess[:10]}")
        new_time = fsolve(f,guess,args=(guess,K1,K2,K3,ct1,ct2,ct3,ct4,cs1,cs2,cs3,cs4,A,B,C1,C2,N_x, vals))
        #print(f"this is the result from fsolve for time step {j}: {new_time[:10]}\n")
        TnS[0,j,:] = new_time[::2]
        TnS[1,j,:] = new_time[1::2]
    
    # return solutions for temperature and fuel
    return TnS[0],TnS[1]
    
