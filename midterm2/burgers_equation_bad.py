"""
Logan Schelly
6 December 2018
"""
import numpy as np
from scipy.linalg import solve, solve_banded
from scipy.sparse import dia_matrix
from scipy.optimize import fsolve

def handle_input(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b):
    """
    Helper function for handling input later on.
    """
    try:
        a = float(a)
    except ValueError:
        raise ValueError("a could not be cast to a float")
    
    try:
        b = float(b)
    except ValueError:
        raise ValueError("b could not be cast to a float")
    
    
    if(a >= b):
        raise ValueError("b should be larger than a")
    
    try:
        T = float(T)
    except ValueError:
        raise ValueError("T could not be cast to a float")
    
    if(T <=0):
        raise ValueError("T should be positive")
    
    if(type(N_x) != int):
        raise TypeError("N_x should be an int")
    
    if(N_x <= 0):
        raise ValueError("N_x should be positive")
    
    if(type(N_t) != int):
        raise TypeError("N_t should be an int")
    
    if(N_t <= 0):
        raise ValueError("N_t should be positive")
        
    if(not callable(u_0)):
        raise TypeError("u_0 should be callable.")
            
    if(not callable(c_a)):
        raise TypeError("c_a should be callable.")
                
    if(not callable(d_a)):
        raise TypeError("d_a should be callable.")  
              
    if(not callable(h_a)):
        raise TypeError("h_a should be callable.")  
              
    if(not callable(c_b)):
        raise TypeError("c_b should be callable.") 
               
    if(not callable(d_b)):
        raise TypeError("d_b should be callable.") 
               
    if(not callable(h_b)):
        raise TypeError("h_b should be callable.")
    
    return

def newton(f, Df, x0, etol=10**-7, maxiters=1000):
    xk = x0
    fxk = f(xk)
    for k in range(maxiters):
    
        #Use the newton iteration formula
        print('Iteration', k, end=' ')
        xkp1 = xk - solve(Df(xk), fxk)
        
        #Check if we're close enough to being a root.
        fxkp1 = f(xkp1)
        print("Norm is", np.linalg.norm(fxkp1))
        if(np.linalg.norm(fxkp1) < etol):
            print("Newton Method Done!\n")
            return xkp1
            
        #Otherwise update for next iteration.    
        else:
            print('', end='\r')
            xk = xkp1
            fxk = fxkp1
    
    raise Exception("Maximum number of iterations exceeded")

def newton_banded(f, Df_formatted, x0, l=1, u=1, etol=10**-4, maxiters=1000):
    """
    Finds roots of a function f using newton's method.  This is specifically
    for when the Jacobian determinant is known to be a banded matrix.
    
    f -- The function you're trying to find the roots of.
    
    Df_formatted -- The jacobian determinant of f, with entries formatted correctly.
                    scipy's solve_banded function likes the banded matrix to be
                    passed in the matrix diagonal ordered form:

                    ab[u + i - j, j] == a[i,j]

                    Example of `ab` (shape of a is (6,6), `u` =1, `l` =2)::

                    *    a01  a12  a23  a34  a45
                    a00  a11  a22  a33  a44  a55
                    a10  a21  a32  a43  a54   *
                    a20  a31  a42  a53   *    *
                    
    x0 -- initial guess
    l -- the number of sub-diagonals that are present
    u -- the number of sup-diagonals that are present
    etol -- distance between f(x) and 0 at which we stop iterating.
    maxiters -- maximum number of iterations of newton's method.
    """
    xk = x0
    fxk = f(xk)
    for k in range(maxiters):
    
        #Use the newton iteration formula
        print('Iteration', k, end=' ')
        xkp1 = xk - solve_banded((l, u), Df_formatted(xk), fxk)
        
        #Check if we're close enough to being a root.
        fxkp1 = f(xkp1)
        print("Norm is", np.linalg.norm(fxkp1))
        if(np.linalg.norm(fxkp1) < etol):
            print("Newton Method Done!\n")
            return xkp1
            
        #Otherwise update for next iteration.    
        else:
            print('', end='\r')
            xk = xkp1
            fxk = fxkp1
    
    raise Exception("Maximum number of iterations exceeded")
    
def burgers_equation(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b):
    """
    a - float
    b - float
    T - positive float
    N_x - positive integer
    N_t - positive integer
    u_0 - function handle for the initial function auxiliary condition
    c_a - function handle 
    d_a - function handle
    h_a - function handle
    c_b - function handle 
    d_b - function handle
    h_b - function handle
    """
    handle_input(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b)
    #Create the grid for the spatial dimension
    x, delta_x = np.linspace(a, b, N_x, retstep=True)
    t, delta_t = np.linspace(0, T, N_t, retstep=True)
    
    #Initialize a matrix to hold the solution in.
    #Rows will increase with time.
    #Columns will increase with space.
    U = np.empty((t.size, x.size))
    
    #Set the first row using the initial condition function.
    U[0, :] = u_0(x)
    
    #Calculate the K1 and K2 that show up in the Crank-Nicholson scheme.
    K1 = delta_t / (4 * delta_x)
    K2 = delta_t / (2 * delta_x**2)
    
    #Iterate through each time step
    for n, tnp1 in enumerate(t[1:]):
        tn = t[n]
        #Each time step results in its own unique non-linear system to solve.
        #Create the function f for our newton solver.
        def f(Unp1):
        
            #Calculate the error where x=a from the boundary conditions
            #Use a forward difference quotient to approximate u_x(a, t)
            U_x = (Unp1[1]-Unp1[0])/delta_t
            error_a = c_a(tnp1)*Unp1[0] + d_b(tnp1)*U_x
            error_a -= h_a(tnp1)
            
            #Calculate the error when a<x<b using the Crank-Nicholson scheme.
            #Use a centered difference quotient to approximate u_t
            error_inside = U[n, 1:-1] - Unp1[1:-1]
            #Take the centered difference quotient for u_x and multiply it by u.
            error_inside -= K1*Unp1[1:-1]*(Unp1[2:] - Unp1[:-2])
            #Average that with the same stuff the previous times step ~ u*u_x
            error_inside += K1*U[n, 1:-1]*(U[n, 2:] - U[n, :-2])
            #Take the centered difference quotient for u_xx when t = t[n+1]
            error_inside += K2*(Unp1[2:] - 2*Unp1[1:-1] + Unp1[:-2])
            #Average that with the centered difference quotient for u_xx when t=t[n]
            error_inside += K2*(U[n, 2:] - 2*U[n, 1:-1] + U[n, :-2])
            #Note: K1 and K2 show up when you multiply both sides by delta_t.
            
            #Calculate the error when x = b
            error_b = c_b(tnp1)*Unp1[-1] + d_b(tnp1)*(Unp1[-1] - Unp1[-2])/delta_t
            error_b -= h_b(tnp1)
            
            #hstack the errors together.
            return np.hstack((error_a, error_inside, error_b))
        """
        #Create the function that gives our formatted Jacobian.
        def Df_formatted(Unp1):
        
            #When column is larger than row, we get this
            sup_diag = -K1*Unp1 + K2
            
            #When on the interior of main diagonal, we get this
            diag_inside = -1 - K1*(Unp1[2:] - Unp1[:-2]) - 2*K2
            
            #Top left entry of main diagonal
            Ca0 = (delta_t * h_a(tnp1))/(delta_t * c_a(tnp1) - d_a(tnp1))
            Ca1 = d_a(tnp1) / (delta_t * c_a(tnp1) - d_a(tnp1)) 
            top_left = -1 -K1*(Unp1[1] - Ca0 + 2*Ca1*Unp1[0]) - K2*(2 + Ca1)
            
            #Bottom right entry of the main diagonal
            Cb0 = (delta_t * h_b(tnp1))/(delta_t * c_b(tnp1) + d_b(tnp1))
            Cb1 = d_b(tnp1) / (delta_t * c_a(tnp1) - d_a(tnp1))
            btm_right = -1 -K1*(Cb0 - Unp1[-2] + 2*Cb1*Unp1[-1]) + K2*(Cb1 - 2)
            
            main_diag = np.hstack((top_left, diag_inside, btm_right))
            
            #When column is less than row, we get this
            sub_diag = K1*Unp1 + K2
            
            #Stack them on top of eachother to create the format necessart.
            return np.vstack((sup_diag, main_diag, sub_diag))
        """
        def Df(Unp1):
            #When column is larger than row, we get this
            sup_diag = -K1*Unp1 + K2
            
            #When on the interior of main diagonal, we get this
            diag_inside = -1 - K1*(Unp1[2:] - Unp1[:-2]) - 2*K2
            
            #Top left entry of main diagonal
            Ca0 = (delta_t * h_a(tnp1))/(delta_t * c_a(tnp1) - d_a(tnp1))
            Ca1 = d_a(tnp1) / (delta_t * c_a(tnp1) - d_a(tnp1)) 
            top_left = -1 -K1*(Unp1[1] - Ca0 + 2*Ca1*Unp1[0]) - K2*(2 + Ca1)
            
            #Bottom right entry of the main diagonal
            Cb0 = (delta_t * h_b(tnp1))/(delta_t * c_b(tnp1) + d_b(tnp1))
            Cb1 = d_b(tnp1) / (delta_t * c_a(tnp1) - d_a(tnp1))
            btm_right = -1 -K1*(Cb0 - Unp1[-2] + 2*Cb1*Unp1[-1]) + K2*(Cb1 - 2)
            
            main_diag = np.hstack((top_left, diag_inside, btm_right))
            
            #When column is less than row, we get this
            sub_diag = K1*Unp1 + K2
            
            #construct the matrix
            shape = (len(main_diag), len(main_diag))
            data = [sup_diag, main_diag, sub_diag]
            offsets = [1, 0, -1]
            J = dia_matrix((data, offsets), shape = shape)
            return J.todense()
            
        #Use the newton method with those two functions to solve the equation.
        #print("Calling newton method when n =", n)
        print ("Calling fsolve when n =", n)
        U[n+1, :] = fsolve(f, U[n, :])#newton(f, Df,U[n, :])
    
    #When done iterating through the columns...
    return U
    
    
