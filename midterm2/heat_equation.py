"""
Created on Tuesday, December 4th 7:19 PM

author: Logan Schelly
"""

import numpy as np
from scipy.sparse import dia_matrix
from scipy.sparse.linalg import spsolve

def heat_equation(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b):
    """
    a - float
    b - float
    T - positive float
    N_x - positive integer
    N_t - positive integer
    u_0 - function handle for the initial function auxiliary condition
    c_a - function handle 
    d_a - function handle
    h_a - function handle
    c_b - function handle 
    d_b - function handle
    h_b - function handle
    """
    #handle incorrect input
    try:
        a = float(a)
    except ValueError:
        raise ValueError("a could not be cast to a float")
    
    try:
        b = float(b)
    except ValueError:
        raise ValueError("b could not be cast to a float")
    
    
    if(a >= b):
        raise ValueError("b should be larger than a")
    
    try:
        T = float(T)
    except ValueError:
        raise ValueError("T could not be cast to a float")
    
    if(T <=0):
        raise ValueError("T should be positive")
    
    if(type(N_x) != int):
        raise TypeError("N_x should be an int")
    
    if(N_x <= 0):
        raise ValueError("N_x should be positive")
    
    if(type(N_t) != int):
        raise TypeError("N_t should be an int")
    
    if(N_t <= 0):
        raise ValueError("N_t should be positive")
        
    if(not callable(u_0)):
        raise TypeError("u_0 should be callable.")
            
    if(not callable(c_a)):
        raise TypeError("c_a should be callable.")
                
    if(not callable(d_a)):
        raise TypeError("d_a should be callable.")  
              
    if(not callable(h_a)):
        raise TypeError("h_a should be callable.")  
              
    if(not callable(c_b)):
        raise TypeError("c_b should be callable.") 
               
    if(not callable(d_b)):
        raise TypeError("d_b should be callable.") 
               
    if(not callable(h_b)):
        raise TypeError("h_b should be callable.")
    
    #Create the grid for the spatial dimension
    x, delta_x = np.linspace(a, b, N_x, retstep=True)
    t, delta_t = np.linspace(0, T, N_t, retstep=True)
    
    #Initialize a matrix to hold the solution in.
    #Rows will increase with time.
    #Columns will increase with space.
    U = np.empty((t.size, x.size))
    
    #Set the first row using the initial condition function.
    U[0, :] = u_0(x)
    
    #(delta_t)/(2 * delta_x**2) shows up a lot in the Crank-Nicolson scheme,
    #so store it. (Heat flow lab labels it as lambda)
    lam = delta_t/(2 * delta_x**2)
    
    #Create the matrix A
    A = dia_matrix((N_x-2, N_x))
    A.setdiag(lam, k=0)
    A.setdiag(1-2*lam, k=1)
    A.setdiag(lam, k=2)
    
    #Crate the matrix B
    B = dia_matrix((N_x-2, N_x-2))
    B.setdiag(-lam, k=-1)
    B.setdiag(-lam, k=1)
    B_main_diag_interior = np.full(N_x-4, 1+2*lam)
    
    #Go through each value of t in the grid, except the first.
    for j, tj in enumerate(t[1:]):
        j= j+1 #off by one error from enumerate.
        
        #Calculate U[j, 1:-1], the interior spatial nodes.
        
        #When 1<i<N_x-1, the Crank-Nicolson scheme uses this finite difference
        #approximation for u_t: 
        #     (U[j, i] - U[j-1, i])/delta_t
        #
        #It approximates u_xx by averaging the finite difference approximation
        #for u_xx when t = t[j-1] with the finite difference approximation for
        #u_xx when t = t[j]:
        #     ((U[j, i+1] - 2U[j, i] + U[j, i-1])/(delta_x**2)
        #       + (U[j-1, i+1] - 2U[j-1, i] + U[j-1, i-1])/(delta_x**2))/2
        #
        #If you plug these approximations in for u_t and u_xx in the heat equation
        #u_t = u_{xx}, multiply everything by delta_t, move everything involving
        #j to one side and everything involving j-1 to the other, and subsitute
        #lam= delta_t/(2 * delta_x**2), you get this:
        #
        # -lam*U[j, i-1] + (1+2lam)U[j, i] - lam*U[j, i+1]
        #        = lam U[j-1, i-1] + (1-2lam)U[j-1, i] - lam U[j-1, i+1]
        #
        #This explains the middle N_x - 4 rows of the diagonal matrix 
        #we use for this method.
        #
        #Surprisingly, we don't create a matrix with N_x rows,
        #but rather a matrix with N_x columns and N_x - 2 rows.  That's because we can
        #Use a finite difference approximation for u_x(a, t) along with the bc
        #at our endpoints to express U[j, 0] in terms of U[j, 1]:
        #
        #  U[j,0]= (d_a(tj)*U[j,1] - delta_x*h_a(tj))/(d_a(tj) - delta_x*c_a(tj))
        #
        #So, we can substitute this expression in to the scheme where i=1 to get
        #the eqution for our first row:
        #
        #(1+lam*(2 -d_a(tj)/(d_a(tj) - delta_x*c_a(tj))))U[j,1]
        # -lam U[j, 2] = lam U[j-1, 0] + (1-2lam)U[j-1, 1] - lam U[j-1, 2]
        # - lam*(d_a(tj)/(d_a(tj)-delta_x*c_a(tj)))
        #
        #
        #Similarly, at the other boundary condition we can use a finite differece
        #approximation for u_x(b, t) to get a similar equation for U[j, -2]
        #
        #The idea is to 
        #
        #  1) Use the matrix A to calculate the stuff on the right hand
        #     side ofe the equation that involves U[j-1, :]
        #
        #  2) When we account for the boundary conditions and re-express
        #     U[j, 0] in terms of U[j, 1], some constant stuff comes along.
        #     Add that to the right hand side after multiplication by A.
        #     Do the same for U[j, -1] and U[j, -2]
        #
        #  3) Account for the stuff that multiplies to U[j, 1] and U[j, -2]
        #     from the boundary conditions by adjusting the matrix B.
        #
        #  4) Use the matrix B to solve the system from the lhs of the equation.
        #     and store the solution in U[j, 1:-1]
        #
        #  5) Use the boundary conditions to solve for U[j, 0] and U[j, -1]
        #     now that we have U[j, 1] and U[j, -2].
        
        #Multiply A to U[j-1, :] to the the right hand side of our equation.
        rhs = A.dot(U[j-1, :])
        
        #Add in the constant stuff from the U[j, 0] boundary condition.
        rhs[0] -= lam * (delta_x * h_a(tj))/(d_a(tj) - delta_x * c_a(tj))
        #Add in the constant stuff from the U[j, -1] boundary condition.
        rhs[-1] -= lam * (delta_x * h_b(tj))/(d_b(tj) + delta_x * c_b(tj))
        
        #Calculate what the top left and bottom right corners of the matrix B
        #should be.
        B_top_left = 1 + lam*(2 - d_a(tj)/(d_a(tj) - delta_x * c_a(tj)))
        B_bottom_right = 1 + lam*(2 + d_b(tj)/(d_b(tj) + delta_x * c_b(tj)))
        B.setdiag(np.hstack((B_top_left, B_main_diag_interior, B_bottom_right)))
        
        #Use B to find U[j, 1:-1]
        U[j, 1:-1] = spsolve(B.tocsr(), rhs)
        
        #Get U[j,0] and U[j,-1] from the boundary conditions.
        U[j,0] = (d_a(tj)*U[j,1] - delta_x*h_a(tj))/(d_a(tj) - delta_x*c_a(tj))
        U[j,-1] = (d_b(tj)*U[j,-1] + delta_x*h_b(tj))/(d_b(tj) + delta_x*c_b(tj))
    
    return U
        
