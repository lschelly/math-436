#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 09:19:34 2018

@author: blake
"""


import numpy as np
import heat_equation as code
import matplotlib.pyplot as plt

a = 0
b = np.pi
T = 1.0
N_x = 30
N_t = 10
u_0 = lambda x: np.sin(x)
c_a = lambda t:1
d_a = lambda t:0
h_a = lambda t:0
c_b = lambda t:1
d_b = lambda t:0
h_b = lambda t:0

# Solution for the heat equation u_t = u_{xx} with u(x,0) = sin(x), u(a,t) = 0, u(b,t) = 0

U = code.heat_equation(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b)

x = np.linspace(a,b,N_x)

plt.plot(x,U[5])
plt.plot(x,U[0])
plt.plot(x,U[9])


