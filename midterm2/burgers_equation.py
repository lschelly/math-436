"""
Logan Schelly
7 December 2018
"""
import numpy as np
from scipy.linalg import solve, solve_banded
from scipy.sparse import dia_matrix
from scipy.optimize import fsolve

def newton_banded(f, Df_formatted, x0, l=1, u=1, etol=10**-4, maxiters=1000):
    """
    Finds roots of a function f using newton's method.  This is specifically
    for when the Jacobian determinant is known to be a banded matrix.
    
    f -- The function you're trying to find the roots of.
    
    Df_formatted -- The jacobian determinant of f, with entries formatted correctly.
                    scipy's solve_banded function likes the banded matrix to be
                    passed in the matrix diagonal ordered form:

                    ab[u + i - j, j] == a[i,j]

                    Example of `ab` (shape of a is (6,6), `u` =1, `l` =2)::

                    *    a01  a12  a23  a34  a45
                    a00  a11  a22  a33  a44  a55
                    a10  a21  a32  a43  a54   *
                    a20  a31  a42  a53   *    *
                    
    x0 -- initial guess
    l -- the number of sub-diagonals that are present
    u -- the number of sup-diagonals that are present
    etol -- distance between f(x) and 0 at which we stop iterating.
    maxiters -- maximum number of iterations of newton's method.
    """
    xk = x0
    fxk = f(xk)
    for k in range(maxiters):
    
        #Use the newton iteration formula
        #print('Iteration', k, end=' ')
        xkp1 = xk - solve_banded((l, u), Df_formatted(xk), fxk)
        
        #Check if we're close enough to being a root.
        fxkp1 = f(xkp1)
        #print("Norm is", np.linalg.norm(fxkp1))
        if(np.linalg.norm(fxkp1) < etol):
            print("Newton Method Done!\n")
            return xkp1
            
        #Otherwise update for next iteration.    
        else:
            #print('', end='\r')
            xk = xkp1
            fxk = fxkp1
    
    raise Exception("Maximum number of iterations exceeded")

class mesh():
    """
    Class for handling all the details of the mesh we create.
    """
    
    def __init__(self, x1, xJ, tN, J, N):
        """
        Initialize a meshgrid object.
        x1 -- First spatial point in the grid.
        xJ -- Last spatial point in the grid.
        tN -- The last time point in the grid (first point is assumed to be 0)
        J -- The number of spatial points
        N -- The number of time points
        """
        self.x_points, self.delta_x = np.linspace(x1, xJ, J, retstep=True)
        self.t_points, self.delta_t = np.linspace(0, tN, N, retstep=True)
    
    @property
    def shape(self):
        """
        Return the number of t points and number of x points as a pair.
        """
        return (self.numberOfTPoints, self.numberOfXPoints)
    
    @property
    def numberOfXPoints(self):
        return self.x_points.size
    
    @property
    def numberOfTPoints(self):
        return self.t_points.size

class burgerBoundaryCondition():
    
    def __init__(self, c, d, h):
        self.c, self.d, self.h = c, d, h
    
    def __call__(self, t):
        return self.c(t), self.d(t), self.h(t)
        
class burgerSolver():
    """
    Class for solving burger's equation.
    """
    
    def __init__(self,a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b):
        
        self.mesh = mesh(a, b, T, N_x, N_t)
        self.boundaryConditionA = burgerBoundaryCondition(c_a, d_a, h_a)
        self.boundaryConditionB = burgerBoundaryCondition(c_b, d_b, h_b)
        self.initial_condition_function = u_0
        self.usingBanded = True
        self.sparseOK = False
        return
    
    def getSolution(self):
        
        #Create an empty matrix the same shape as the mesh.
        print("Initializing the matrix.")
        U = self._initializeU()
        print("getSolution: The type of U is", type(U))
        #Fill in the subsequent rows of U.
        print("Done initializing.")
        self._fillInU(U)
        
        #return U.
        return U
        
    
    def _initializeU(self):
    
        #initialize an empty matrix the same shape as the grid
        U = np.empty(self.mesh.shape)
        print("The type of U is", type(U))
        
        #Use the initial condition function to set up the first row of U. 
        u0 = self.initial_condition_function
        U[0,:] = u0(self.mesh.x_points)
        return U

    def _fillInU(self,U):
        
        #Iterate through each time step after the first one.
        print("_fillInU: The type of U is", type(U))
        N = self.mesh.numberOfTPoints
        for n in range(0, N-1):
            print("Filling in row", n+2)
            guess = U[n,:] #Use the previous configurationas the guess.
            #Use a non-linear solver to figure out what the next row should be.
            f = lambda x : self._rootFunc(x, n, U)
            Df = lambda x : self._rootFuncJac(x, n, U)
            U[n+1, :] = newton_banded(f, Df, guess)
        
    
    def _rootFunc(self, Unp1, n, U):
        
        #Just write Unp1 into U to make it more readable.
        U[n+1,:] = Unp1
        
        #Grab the mesh information we need.
        t = self.mesh.t_points[n+1]
        dt = self.mesh.delta_t
        dx = self.mesh.delta_x
        
        #Calculate the error when x=a using the Robin conditions.
        ca, da, ha = self.boundaryConditionA(t) #Evaluate each function at t.
        u = U[n+1, 0] #u(a, t) approximation.
        u_x = (U[n+1, 0] - U[n+1, 1])/dx #forward difference quotient.
        error_a = ca*u + da*u_x - ha #robin condition, ha(t) subtracted over.
        
        #Calculate the error on the interior spatial points.
        
        #-u_t*dt -- Approximate with a backward difference quotient
        er_int = U[n, 1:-1] - U[n+1, 1:-1]
        
        #-u*u_x*dt -- Approximate as the average of the centered difference
        #quotients on the current time step and the future time step.
        K1 = dt/(4*dx) #one two in denom from avg.  Other from centered.
        er_int -= K1*(U[n+1,1:-1]*(U[n+1,2:] - U[n+1,:-2])) #uu_x*dt/2 future
        er_int -= K1*(U[n,  1:-1]*(U[n,  2:] - U[n,  :-2])) #uu_x*dt/2 current
        
        #u_xx -- Approximate as the average of the finite difference
        #approxmiation on the current time step and the future time step.
        K2 = dt/(2 * dx**2) #two in denom from avg.
        er_int += K2*(U[n+1,2:] - 2*U[n+1,1:-1] + U[n+1,:-2])#u_xx*dt/2 future
        er_int += K2*(U[n,  2:] - 2*U[n,  1:-1] + U[n,  :-2])#u_uxx*dt/2 current
        error_interior = er_int
        
        #Calculate the error when x=b
        cb, db, hb = self.boundaryConditionB(t)
        u = U[n+1, -1]
        u_x = (U[n+1, -2] - U[n+1, -1])/dx
        error_b = cb*u + db*u_x - hb
        
        return np.hstack((error_a, error_interior, error_b))
        
    def _rootFuncJac(self, Unp1, n, U):
        
        #Just write Unp1 into U to make it more readable.
        U[n+1,:] = Unp1
        
        #Grab the mesh information we need.
        t = self.mesh.t_points[n+1]
        dt = self.mesh.delta_t
        dx = self.mesh.delta_x
        K1 = dt/(4*dx)
        K2 = dt/(2 * dx**2)
        ca, da, ha = self.boundaryConditionA(t)
        cb, db, hb = self.boundaryConditionB(t)
        
        #Calculate the main diagonal of the jacobian.
        diag_int = -1 - K1*(U[n+1,2:]-U[n+1,:-2]) - 2*K2
        diag_top_left = ca - da/dx
        diag_btm_right = cb + db/dx
        main_diag = np.hstack((diag_top_left, diag_int, diag_btm_right))
        
        #Calculate the superdiagonal of the jacobian.
        sup_diag = K1*U[n+1,:] + K2
        sup_diag[0] = da/dx#Robin condition at x=a changes first row.
        
        #Calculate the subdiagonal of the jacobian
        sub_diag = -K1*U[n+1,:] + K2
        sub_diag[-1] = -db/dx#Robin condition at x=b changes last row.
        
        if(self.usingBanded):
            return np.vstack((sup_diag, main_diag, sub_diag))
        else:
            #prepend to sup_diag, because sparse matrices chop super at the start.
            sup_diag = np.append(0, sup_diag)
            
            #Construct a sparse diagonal matrix.
            data = [sup_diag, main_diag, sub_diag]
            offsets = [-1, 0 , 1]
            Jshape = (main_diag.size, main_diag.size)
            J = dia_matrix((data, offsets), shape=Jshape)
            
            if(self.sparseDiagOK):
                return J
            else:
                return J.todense()

def handle_input(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b):
    """
    gracefully handle input
    """
    try:
        a = float(a)
    except ValueError:
        raise ValueError("a could not be cast to a float")
    
    try:
        b = float(b)
    except ValueError:
        raise ValueError("b could not be cast to a float")
    
    
    if(a >= b):
        raise ValueError("b should be larger than a")
    
    try:
        T = float(T)
    except ValueError:
        raise ValueError("T could not be cast to a float")
    
    if(T <=0):
        raise ValueError("T should be positive")
    
    if(type(N_x) != int):
        raise TypeError("N_x should be an int")
    
    if(N_x <= 0):
        raise ValueError("N_x should be positive")
    
    if(type(N_t) != int):
        raise TypeError("N_t should be an int")
    
    if(N_t <= 0):
        raise ValueError("N_t should be positive")
        
    if(not callable(u_0)):
        raise TypeError("u_0 should be callable.")
            
    if(not callable(c_a)):
        raise TypeError("c_a should be callable.")
                
    if(not callable(d_a)):
        raise TypeError("d_a should be callable.")  
              
    if(not callable(h_a)):
        raise TypeError("h_a should be callable.")  
              
    if(not callable(c_b)):
        raise TypeError("c_b should be callable.") 
               
    if(not callable(d_b)):
        raise TypeError("d_b should be callable.") 
               
    if(not callable(h_b)):
        raise TypeError("h_b should be callable.")
    
    return
    
    
def burgers_equation(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b):
    """
    a - float
    b - float
    T - positive float
    N_x - positive integer
    N_t - positive integer
    u_0 - function handle for the initial function auxiliary condition
    c_a - function handle 
    d_a - function handle
    h_a - function handle
    c_b - function handle 
    d_b - function handle
    h_b - function handle
    """
    bur_solver = burgerSolver(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b)
    return bur_solver.getSolution()
