import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from burgers_equation import burgers_equation
import sys

#Define the constants and get the 
u_minus =5
u_plus = 1
s = (u_minus + u_plus)/2
a_lab = (u_minus - u_plus)/2
nu = 1 #for professor Barker's problem

#traveling wave solution
delta = 25
u_analytic = lambda x, t : s - (u_minus - u_plus)/2 * np.tanh(a_lab*(x-s*t)/(2*nu) + delta)

x = np.linspace(-20, 20, 400)
t = np.linspace(0, 40, 400)

X,T = np.meshgrid(x, t)
U_an = u_analytic(X, T)

#Initial conditions for numeric solution.
N_x = 400
N_t = 400
a = -20
b = 20
T = 40
u_0  = lambda x_ : u_analytic(x_, 0) #Have the same initial configuration as the analytic solution.
plt.plot(x, U_an[0, :])
plt.plot(x, u_0(x))
plt.show()

c_a = lambda t : 1 # coeficient on u(a, t) is 1
d_a = lambda t : 0 # coefficient on u_x(a, t) is 0
h_a = lambda t : u_analytic(a, t) #Agree with the analytic solution on the boundary.
c_b  = lambda t : 1 #coefficient on u(b, t) is 1
d_b  = lambda t : 0 #coefficient on u_x(b, t) is 0
h_b = lambda t : u_analytic(b, t) #Agree with the analytic solution on the boundary.

print("Calling the numeric solution")
U_num = burgers_equation(a,b,T,N_x,N_t,u_0,c_a,d_a,h_a,c_b,d_b,h_b)
print("Back from numeric solution")

#make figure
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_xlim((-20,20))
ax.set_ylim((0, 6))

#make lines
analytic_line, = ax.plot([], [], label="Analytic")
numeric_line, = ax.plot([], [], label="Crank-Nicholson")

#update function.
def update(i):
    analytic_line.set_data(x, U_an[i, :])
    numeric_line.set_data(x, U_num[i, :])
    return analytic_line,

ani = animation.FuncAnimation(fig, update, frames = len(t), interval=25)
plt.legend()
ani.save("analytic_to_numeric_comparison.mp4")
